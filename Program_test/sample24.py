#program to swap two numbers using call by reference
x = 5
y = 10
x, y = y, x
print("x =", x)
print("y =", y)
