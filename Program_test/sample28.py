#program to find Fibonacci number using recursion
def rec(l,c,d,n):
    sum = c+d
    c=d
    d=sum
    l.append(d)
    n=n-1
    if(n>0):
      return rec(l,c,d,n)
    else:
        return l
a=1
b=2
n=10
l1=[]
l2=[]
l1.append(a)
l1.append(b)
l2 = rec(l1,a,b,n)
print(l2)
