#################################################################################################################


""" Preamble"""

import pandas as pd
import numpy as np
from collections import Counter
import joblib
import nltk
from nltk.stem import WordNetLemmatizer 
from sklearn.feature_extraction.text import TfidfVectorizer
import sys, token, tokenize, re
import os
import csv
import spacy
from nltk import pos_tag, word_tokenize
import inspect, importlib as implib
import dis
from collections import defaultdict
from pprint import pprint
from itertools import groupby

wnl = WordNetLemmatizer() 
directory = os.getcwd()

#################################################################################################################


""" Sample User Interface """

lang = "python"  #can be modified with the development of code - can be automated in the future
filename = input("Enter the name of the file: ")
file_name =(directory+"/Program_test/"+filename)
#comments = input("Enter any extra file descriptions and comments: ")
#can be uncommented in order to give the user flexibility of adding tags, subsquently, uncomment the next line
comments = ""


#################################################################################################################


""" Extracting comments works for C and Python"""

def for_py(filename):
    comment_list = []
    full_code =[]
    with open(filename) as source: # open("new_" + filename, "w") as mod:    
        prev_toktype = token.INDENT
        first_line = None
        last_lineno = -1
        last_col = 0

        indent_stack = []
        tokgen = tokenize.generate_tokens(source.readline)
    
        for toktype, ttext, (slineno, scol), (elineno, ecol), ltext in tokgen: 
            full_code.append(ltext)
            if toktype == token.INDENT:
                indent_stack.append(toktype)
            elif toktype == token.DEDENT:
                indent_stack.pop()
            
            '''
            if slineno > last_lineno:
                last_col = 0
            if scol > last_col:
                if last_col == 0 and len(indent_stack):
                    mod.write("\t")
                mod.write(" " * (scol - last_col))
            '''
            if (toktype == token.STRING and prev_toktype == token.INDENT) or toktype == tokenize.COMMENT: # Docstring or Comment
                comment = ttext.replace("\n", " ").replace("\t", " ").replace('"""', "").replace("#", "").strip()
                comment_list.append(comment)
            else:
                if(ttext != '\n'):
                    #print(ttext)
                    comment_list.append("////")
            
            prev_toktype = toktype
            last_col = ecol
            last_lineno = elineno
            
            #print(comment_list)
            #print(full_code)
    return comment_list,full_code

def extract_py(source_file):
    comment_list = []
    mod = ""

    
    prev_toktype = token.INDENT
    first_line = None
    last_lineno = -1
    last_col = 0

    indent_stack = []
    tokgen = tokenize.generate_tokens(source)
    for toktype, ttext, (slineno, scol), (elineno, ecol), ltext in tokgen: 

            if toktype == token.INDENT:
                indent_stack.append(toktype)
            elif toktype == token.DEDENT:
                indent_stack.pop()

            if slineno > last_lineno:
                last_col = 0
            if scol > last_col:
                if last_col == 0 and len(indent_stack):
                    mod.write("\t")
                mod.write(" " * (scol - last_col))
            
            if (toktype == token.STRING and prev_toktype == token.INDENT) or toktype == tokenize.COMMENT: # Docstring or Comment
                comment = ttext.replace("\n", " ").replace("\t", " ").replace('"""', "").replace("#", "").strip()
                comment_list.append(comment)
            else: # Regular Token
                mod.write(ttext)

            prev_toktype = toktype
            last_col = ecol
            last_lineno = elineno

    return comment_list


def for_c(filename):
    comment_list = []
    def replacer(match):
        s = match.group(0)
        comment = s.replace("/", "").replace("*", "").replace("\n", "").strip()
        comment_list.append(comment)
        if s.startswith('/'):
            return "" 
        else:
            return s

    with open(filename) as source, open("new_" + filename, "w") as mod:
        text = source.read()

        pattern = re.compile(
            r'//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
            re.DOTALL | re.MULTILINE
        )

        decommented = re.sub(pattern, replacer, text)
        mod.write(decommented)

    return comment_list

def extract_c(source_file):
    comment_list = []
    mod = ""

    def replacer(match):
        s = match.group(0)
        comment = s.replace("/", "").replace("*", "").replace("\n", "").strip()
        comment_list.append(comment)
        if s.startswith('/'):
            return "" 
        else:
            return s

    text = source_file

    pattern = re.compile(
        r'//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
        re.DOTALL | re.MULTILINE
    )

    decommented = re.sub(pattern, replacer, text)
    mod += decommented

    return mod, comment_list

comm=[]
imp=[]

if __name__ == '__main__':


    if lang == "python":
        #print(*for_py(file_name))
        comm,imp = for_py(file_name)
        #print(comm)
        #print(imp)
        '''with open(file_name) as source:
            text = source.read()
        decommented, extracted = extract_py(text)

        print(extracted)'''
    
    elif lang == "C":
        print(*for_c(file_name))

        '''with open(file_name) as source:
            text = source.read()
        decommented, extracted = extract_c(text)
        
        print(extracted)'''
        comm = for_c(file_name)

comm.append(comments)

idx = comm.index('////')
print(comm[:idx])
comm = comm[:idx]

res = [i[0] for i in groupby(imp)]

group = ''.join([res[0]] + [item for item in res[1:]])
#print(group)

instructions = dis.get_instructions(group)
imports = [__ for __ in instructions if 'IMPORT' in __.opname]
grouped = defaultdict(list)
for instr in imports:
   grouped[instr.opname].append(instr.argval)


pprint(grouped)


#################################################################################################################

""" Loading saved NER model and predicting """

#NER model is present in the directory 

indices = []
output_dir = "./model1" # Set the output directory
#print("Loading from", output_dir)
ner_model = spacy.load(output_dir)
for comment in comm:
    #sprint(pos_tag(word_tokenize(comment)))
    doc = ner_model(comment)
    
    prediction = [(ent.text, ent.label_) for ent in doc.ents]
    #print("pred",prediction)
    #print("Predicted Entities:",prediction)
    #print()

    index1=0
    for i in prediction:
        indices.append(i[0])


wt_set = []
for i in indices: 
    wt_set.append(pos_tag(word_tokenize(i)))
    

#print(wt_set)

lemma = []
for i in wt_set:
    for word, tag in i:
        word1 = word.lower()
        #print(tag,word)
        wntag = tag[0].lower()
        wntag = wntag if wntag in ['a', 'r', 'n', 'v'] else None
        if not wntag:
                lemma.append(word)
        else:
                lemma.append(wnl.lemmatize(word1, wntag))

    
exceptions = ["code","function","python","this","it","is","program"]  #exceptions can be added while training the model or manually

final_tags = []
for i in lemma:
    i = i.lower()
    if i not in exceptions:
        #print(i)
        final_tags.append(i)

#print("final tags: ",set(final_tags))
        
'''
tagger = nltk.pos_tag(indices)
print(tagger)
for i in tagger:
    #print(i)
    #j= i[0].lower()
    print(indi.append(lemmatizer.lemmatize(j,i[1].lower())))
indi = set(indi)

print("These are the identified tags: ")
print(indi)
final_tags = input("Select those applicable: ")
'''
    
################################################################################################################


"""Creating the database - adding the tags into the .csv file"""

#cols = ["Import files","Comments"]
#entry = {"Import files":[grouped],"Comments":[comm]}
cols = ["File_name","Tags","Import files","Comments"]
entry = {"File name": filename ,"Tags": [final_tags],"Import files":[grouped],"Comments":[comm]}
df2 = pd.DataFrame(entry)

if not os.path.isfile(directory+"\database_test.csv"):
        
       df2.to_csv(directory+"\database_test.csv", header='column_names', index = 0)
else:
       df2.to_csv(directory+"\database_test.csv", mode='a', header=False, index = 0)


################################################################################################################
